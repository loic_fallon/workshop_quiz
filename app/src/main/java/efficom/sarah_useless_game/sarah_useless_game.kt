package efficom.sarah_useless_game

import android.app.Application
import timber.log.Timber

class sarah_useless_game: Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }
}