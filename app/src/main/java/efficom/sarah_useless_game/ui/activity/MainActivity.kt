package efficom.sarah_useless_game.ui.activity

import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import efficom.sarah_useless_game.R
import efficom.sarah_useless_game.model.Question
import efficom.sarah_useless_game.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.defeat_dialog.view.*
import kotlinx.android.synthetic.main.success_dialog.view.*
import timber.log.Timber
import java.util.*


class MainActivity : AppCompatActivity() {

    val viewModel = ResponseViewModel()
    lateinit var actualQuestion: Question
    lateinit var layout : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObserver()
        initEasterEgg()
        viewModel.getQuestion()
    }

    fun initObserver(){
        viewModel.getQuestionResult.observe(this, Observer {
            initBtn()
        })
    }

    fun initBtn(){
        btn1.setOnClickListener {
            newQuestion(1)
        }
        btn2.setOnClickListener {
            newQuestion(2)
        }
        btn3.setOnClickListener {
            newQuestion(3)
        }
        btn4.setOnClickListener {
            newQuestion(4)
        }
    }

    fun initEasterEgg(){
        var click = 0
        imageView.setOnClickListener {
            click += 1
            if (click == 20){
                val mediaPlayer = MediaPlayer.create(this, R.raw.baptiste)
                mediaPlayer.start()
                click = 0
            }
        }
    }

    fun newQuestion(type: Int){
        val question = viewModel.getQuestionByType(type)
        when(question){
            null    -> Timber.d("Plus de question")
            else    -> {
                actualQuestion = question
                displayQuestion()
            }
        }
    }

    fun displayQuestion(){
        changeView(2)
        questionLayout.removeAllViews()

        textView2.textSize = 25F
        textView2.text = actualQuestion.question.capitalize()

        var textSize = 14F
        var maxSize = 0
        actualQuestion.listOfResponse.forEach {
            if (it.length > maxSize){
                maxSize = it.length
            }

            if (maxSize > 40){
                textSize = 12F
            }
            else if (maxSize in 40 until 60){
                textSize = 10F
            }
        }

        actualQuestion.listOfResponse.forEach {response ->

            val rnd = Random()
            val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

            var lparams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
            )
            lparams.setMargins(0, 36, 0, 36)
            lparams.width = 450
            lparams.height = 150

            val tv = Button(this)
            tv.layoutParams = lparams
            tv.text = response.capitalize()
            tv.setTextColor(resources.getColor(R.color.background))
            tv.textSize = textSize
            tv.setPadding(16,0,16,0)

            tv.setBackgroundColor(color)
            questionLayout.addView(tv)

            tv.setOnClickListener {
                validateResponse(response)
            }
        }
    }

    fun changeView(view: Int){

        val fadeOut = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_out)
        val fadeIn = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)

        /**ObjectAnimator.ofFloat(imageView, "translationX", -300F).apply {
            duration = 2000
            start()
            ObjectAnimator.ofFloat(imageView, "translationY", -125F).apply {
                duration = 2000
                start()
            }

        }**/

        if (view == 2){
            btn1.startAnimation(fadeOut)
            btn2.startAnimation(fadeOut)
            btn3.startAnimation(fadeOut)
            btn4.startAnimation(fadeOut)
            btn1.visibility = View.GONE
            btn2.visibility = View.GONE
            btn3.visibility = View.GONE
            btn4.visibility = View.GONE

            questionLayout.visibility = View.VISIBLE
            questionLayout.startAnimation(fadeIn)

        }else{
            textView2.text = resources.getString(R.string.theme)
            btn1.visibility = View.VISIBLE
            btn2.visibility = View.VISIBLE
            btn3.visibility = View.VISIBLE
            btn4.visibility = View.VISIBLE
            btn1.startAnimation(fadeIn)
            btn2.startAnimation(fadeIn)
            btn3.startAnimation(fadeIn)
            btn4.startAnimation(fadeIn)


            questionLayout.visibility = View.GONE
            questionLayout.startAnimation(fadeOut)
        }
    }

    fun validateResponse(prop: String){
        if (actualQuestion?.response == prop){
            displayDialog(1)
        }else{
            displayDialog(2)
        }
    }

    fun displayDialog(response: Int){
        val builder = AlertDialog.Builder(this).create()

        if (response == 1){
            layout = LayoutInflater.from(this).inflate(R.layout.success_dialog, null)
            layout.ok_success.setOnClickListener{
                changeView(1)
                builder.dismiss()
            }
        }
        else{
            layout = LayoutInflater.from(this).inflate(R.layout.defeat_dialog, null)
            val response = resources.getString(R.string.response) + " " + actualQuestion.response
            layout.response.text = response
            layout.ok_defeat.setOnClickListener {
                changeView(1)
                builder.dismiss()
            }
        }

        builder.setView(layout)
        builder.setCancelable(false)

        builder.show()
    }

    fun toast(text: String){
        Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
    }
}
