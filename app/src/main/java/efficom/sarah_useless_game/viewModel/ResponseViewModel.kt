package efficom.sarah_useless_game.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import efficom.sarah_useless_game.model.Question
import timber.log.Timber
import javax.inject.Inject

class ResponseViewModel : ViewModel() {

    private val listOfQuestion = mutableListOf<Question>()

    val _getQuestionResult = MutableLiveData<Boolean>()
    val getQuestionResult = _getQuestionResult

    fun getQuestion(){

        val r1 = mutableListOf<String>()
        r1.add("1976")
        r1.add("1970")
        r1.add("1985")
        val q1 = Question("1", "Quelle est la date de création de Décathlon ?", 1, r1, "1976")
        listOfQuestion.add(q1)

        val r11 = mutableListOf<String>()
        r11.add("2020")
        r11.add("2067")
        r11.add("JAMAIS")
        val q11 = Question("1", "En quelle année l'entreprise Décathlon va t-elle faire faillite ?", 1, r1, "JAMAIS")
        listOfQuestion.add(q11)

        val r2 = mutableListOf<String>()
        r2.add("Villeneuve-d'Ascq")
        r2.add("Paris")
        r2.add("Bordeaux")
        val q2 = Question("1", "Ou se situe notre siège social ?", 2, r2, "Villeneuve-d'Ascq")
        listOfQuestion.add(q2)

        val r21 = mutableListOf<String>()
        r21.add("18-25 ans")
        r21.add("25-45 ans")
        r21.add("Tout le monde sauf Donald Trump")
        val q21 = Question("1", "Quel est la cible de Décathlon ?", 3, r21, "Tout le monde sauf Donald Trump")
        listOfQuestion.add(q21)

        val r3 = mutableListOf<String>()
        r3.add("Gérard Mulliez")
        r3.add("Michel Leclercq")
        r3.add("Gregory Renuy")
        val q3 = Question("1", "Quel est le nom du fondateur du groupe Décathlon ?", 3, r3, "Michel Leclercq")
        listOfQuestion.add(q3)

        val r4 = mutableListOf<String>()
        r4.add("Honnetete et confiance")
        r4.add("Transparence et respect")
        r4.add("Vitalité et responsabilté")
        val q4 = Question("1", "Quelles sont les valeurs de notre entreprise ?", 3, r4, "Vitalité et responsabilté")
        listOfQuestion.add(q4)

        val r5 = mutableListOf<String>()
        r5.add("15 milliards d'euros")
        r5.add("11,3 milliards d'euros")
        r5.add("10 milliards d'euros")
        val q5 = Question("1", "Quel est le chiffre d'affaire annuel de Décathlon ?", 4, r5, "11,3 milliards d'euros")
        listOfQuestion.add(q5)

        val r6 = mutableListOf<String>()
        r6.add("blanc, bleu")
        r6.add("blanc, bleu, noir")
        r6.add("blanc, vert, jaune")
        val q6 = Question("1", "Quelles sont les couleurs de notre logo ?", 3, r6, "blanc, bleu")
        listOfQuestion.add(q6)

        val r7 = mutableListOf<String>()
        r7.add("Just do it !")
        r7.add("You can !")
        r7.add("À fond la forme !")
        val q7 = Question("1", "Quel est son slogan de Décathlon ? ", 3, r7, "À fond la forme !")
        listOfQuestion.add(q7)

        val r8 = mutableListOf<String>()
        r8.add("315")
        r8.add("280")
        r8.add("400")
        val q8 = Question("1", "Quel est le nombre de magasins Decathlon en France ? ", 4, r8, "315")
        listOfQuestion.add(q8)

        val r10 = mutableListOf<String>()
        r10.add("90000")
        r10.add("80000")
        r10.add("75000")
        val q10 = Question("1", "Quel est l’effectif actuel de l'entreprise? ", 4, r10, "90000")
        listOfQuestion.add(q10)

        _getQuestionResult.postValue(true)
    }

    fun getQuestionByType(type: Int): Question{
        var question = getRandomQuestion()
        while(question.type != type){
            question = getRandomQuestion()
        }
        return question
    }

    fun getRandomQuestion(): Question{
        listOfQuestion?.let {
            val rng = (0 until listOfQuestion.size).random()
            return it[rng]
        }
    }
}